# :space_invader: NFT (ERC721)
Basic implementation of ERC721 token standart</br> 
**ERC721 ETHERSCAN:** https://rinkeby.etherscan.io/address/0x9f770a56b1087761fe05f29a0c228894110efaee#code </br> 
**OPENSEA(testnet):** https://testnets.opensea.io/0x9f770a56b1087761fe05f29a0c228894110efaee </br>

## Tasks
- accounts (standart task to get accounts)
- mint (to mint token to owner with json scheme)
