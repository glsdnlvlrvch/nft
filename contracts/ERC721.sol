// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract DHeroes is ERC721, Ownable, IERC721Receiver {
    using Counters for Counters.Counter;

    Counters.Counter private tokenIds;
    mapping(uint256 => string) private _tokenURIs;

    modifier onlyTokenOwner(uint256 _tokenId) {
        require(ownerOf(_tokenId) == _msgSender(), "not your token");
        _;
    }

    constructor() ERC721 ("DOTA 2 Heroes", "DOTA2_HEROES") {}

    function tokenURI(uint256 _tokenId) public view override returns (string memory) {
        require(_exists(_tokenId), "invalid tokenId");
        string memory _tokenURI = _tokenURIs[_tokenId];

        return _tokenURI;
    }

    function mint(
        address _owner, 
        string memory _tokenURI
    ) public onlyOwner {
        _safeMint(_owner, tokenIds.current());
        _setTokenURI(tokenIds.current(), _tokenURI);

        tokenIds.increment();
    }

    function _setTokenURI(
        uint256 tokenId, 
        string memory _tokenURI
    ) internal {
        require(_exists(tokenId), "ERC721URIStorage: URI set of nonexistent token");
        _tokenURIs[tokenId] = _tokenURI;
    }

    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) public override returns (bytes4) {
        return IERC721Receiver.onERC721Received.selector;
    }
}

