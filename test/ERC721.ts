import { expect } from "chai";
import { ethers } from "hardhat";

import { Contract, Signer, BigNumber } from "ethers";


describe("NFT(ERC721)", function () {
    let token: Contract;
    let signers: Signer[];

    beforeEach(async function () {
        signers = await ethers.getSigners();

        const Token = await ethers.getContractFactory("DHeroes");
        token = await Token.deploy();
        await token.deployed();
    });

    it("mint() and tokenURI()", async () => {
        let addr = await signers[0].getAddress();

        await expect(
            token.tokenURI(0)
        ).to.be.revertedWith("invalid tokenId");

        await token.mint(addr, "fake_json");

        expect(
            await token.balanceOf(addr)
        ).to.be.eq(1);

        expect(
            await token.tokenURI(0)
        ).to.be.eq("fake_json");
    });
})